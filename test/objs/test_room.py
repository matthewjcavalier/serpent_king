from serpent_king.objs.room import Room, room_to_points
from serpent_king.objs.coordinate import Coordinate


def test_room_to_points():
    room_set = room_to_points(Room(Coordinate(0, 0), 2, 2))
    for y in range(0, 2):
        for x in range(0, 2):
            assert Coordinate(y, x) in room_set


def test_room_overlaps_self_true():
    room1 = Room(Coordinate(0, 0), 5, 5)
    assert room1.overlaps(room1)


def test_room_overlaps_no_buffer():
    room1 = Room(Coordinate(0, 0), 5, 5)
    room2 = Room(Coordinate(4, 4), 2, 2)

    assert room1.overlaps(room2)
    assert room2.overlaps(room1)


def test_room_overlaps_with_buffer():
    room1 = Room(Coordinate(0, 0), 5, 5)
    room2 = Room(Coordinate(5, 5), 5, 5)

    assert room1.overlaps(room2)
    assert room2.overlaps(room1)


def test_room_overlaps_false_next_to_each_other():
    room1 = Room(Coordinate(0, 0), 3, 3)
    room2 = Room(Coordinate(0, 4), 3, 3)

    assert not room1.overlaps(room2)
    assert not room2.overlaps(room1)


def test_room_overlaps_false_far_apart():
    room1 = Room(Coordinate(10, 10), 3, 3)
    room2 = Room(Coordinate(0, 0), 3, 3)

    assert not room1.overlaps(room2)
    assert not room2.overlaps(room1)


if __name__ == "__main__":
    test_room_overlaps_false_next_to_each_other()
