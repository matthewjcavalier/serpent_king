from serpent_king.objs.coordinate import Coordinate

def test_creation():
    y = 5
    x = 4
    coord = Coordinate(y,x)

    assert y == coord.y
    assert x == coord.x


def test_comparison_true():
    coord1 = Coordinate(1,1)
    coord2 = Coordinate(1,1)

    assert coord1.equals(coord2)


def test_comparison_false():
    coord1 = Coordinate(1,1)
    coord2 = Coordinate(2,2)

    assert not coord1.equals(coord2)
