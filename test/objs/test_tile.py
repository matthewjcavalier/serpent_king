from serpent_king.objs.tile import Tile, TileType
import pytest


def test_creation_room_tile_fail():
    with pytest.raises(ValueError):
        Tile(TileType.ROOM, 1)


def test_creation_room_tile():
    tile = Tile(TileType.ROOM, 0)

    assert 0 == tile.hardness
    assert tile.tile_type is TileType.ROOM


def test_creation_hall_tile_fail():
    with pytest.raises(ValueError):
        Tile(TileType.HALL, 1)


def test_creation_hall_tile():
    tile = Tile(TileType.HALL, 0)

    assert 0 == tile.hardness
    assert tile.tile_type is TileType.HALL


def test_creation_tile_hardness_fail():
    with pytest.raises(ValueError):
        Tile(TileType.ROCK, -1)


def test_thing():
    assert 1 == 1
