from typing import List


class Coordinate(object):

    __slots__ = ["x", "y"]

    def __init__(self, y, x):
        super(Coordinate, self).__setattr__("x", x)
        super(Coordinate, self).__setattr__("y", y)

    def __setattr__(self, name, value):
        raise AttributeError(f"{self.__class__} has no attirbute {name}")

    def equals(self, other):
        return (self.__getattribute__("x") == other.x and
                self.__getattribute__("y") == other.y)

    def __eq__(self, other):
        return self.equals(other)

    def __hash__(self):
        return hash((self.__getattribute__("x"), self.__getattribute__("y")))

    def to_s(self):
        """
        Convert internal state to string
        """
        return f"y:{self.__getattribute__('y')},\
                 x:{self.__getattribute__('x')}"


def get_neighbors(coord: Coordinate) -> List[Coordinate]:
    """
    Get all neighbors from the cardinal directions
    (up, down, left, right) can include invalid locations
    (i.e. outside of dungeon)

    Parameters
    ----------
    coord : Coordinate
        central point

    Returns
    -------
    List[Coordinate]:
        all possible neighbors
    """
    return [Coordinate(coord.y - 1, coord.x),
            Coordinate(coord.y + 1, coord.x),
            Coordinate(coord.y, coord.x - 1),
            Coordinate(coord.y, coord.x + 1)]
