
class Settings:
    def __init__(self):
        self.rows = None
        self.cols = None
        self.min_rooms = None
        self.buffer_rooms = None
        self.min_room_height = None
        self.min_room_width = None
        self.room_height_buffer = None
        self.room_width_buffer = None
        self.room_char = None
        self.hall_char = None
        self.border_char = None
        self.rock_char = None
        self.max_hardness = None
        
        self.player_symbol = "@"
        self.player_hp = 50


def build_settings() -> Settings:
    settings = Settings()
    settings.rows = 21
    settings.cols = 80
    settings.min_rooms = 5
    settings.buffer_rooms = 5
    settings.min_room_height = 4
    settings.min_room_width = 4
    settings.room_height_buffer = 5
    settings.room_width_buffer = 5
    settings.room_char = '.'
    settings.hall_char = '#'
    settings.border_char = '+'
    settings.rock_char = ' '
    settings.max_hardness = 50

    return settings


def build_tiny() -> Settings:
    settings = build_settings()
    settings.rows = 10
    settings.cols = 10
    settings.min_rooms = 2
    settings.buffer_rooms = 0
    settings.min_room_height = 2
    settings.min_room_width = 2
    settings.max_hardness = 9

    return settings


def build_big() -> Settings:
    settings = build_settings()
    settings.rows = 42
    settings.cols = 160
    settings.min_rooms = 20
    settings.buffer_rooms = 10
    settings.min_room_height = 5
    settings.min_room_width = 5
    settings.max_hardness = 9
    settings.room_height_buffer = 5
    settings.room_width_buffer = 5

    return settings
