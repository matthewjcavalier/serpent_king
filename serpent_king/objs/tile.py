from enum import Enum, unique


@unique
class TileType(Enum):
    HALL = 1
    ROOM = 2
    ROCK = 3
    BORDER = 4


class Tile:

    def __init__(self, tile_type: TileType, hardness: int):
        if hardness < 0:
            raise ValueError('Hardness cannot be less than zero')
        if hardness > 0 and \
                (tile_type is TileType.HALL or tile_type is TileType.ROOM):
            raise ValueError(f'Tile of type <{tile_type}> cannot have hardness \
                    of greater than 0')

        self.tile_type = tile_type
        self.hardness = hardness
