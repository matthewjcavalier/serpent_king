from serpent_king.objs.settings import Settings
from serpent_king.objs.tile import Tile, TileType
from serpent_king.objs.room import Room, build_rand_room, room_to_points
from serpent_king.objs.coordinate import Coordinate, get_neighbors
from typing import List
import random
from math import sqrt
from dataclasses import dataclass, field
from queue import PriorityQueue


class Dungeon:
    def __init__(self, settings: Settings):
        self.settings = settings
        self.floor = None
        self.rooms = []

    def __y_max__(self):
        return len(self.floor) - 1

    def __x_max__(self):
        return len(self.floor[0]) - 1

    def get_rand_open_loc(self):
        coord = Coordinate(random.randint(1, self.__y_max__()),
                           random.randint(1, self.__x_max__()))
        while self.floor[coord.y][coord.x].hardness > 0:
            coord = Coordinate(random.randint(1, self.__y_max__()),
                               random.randint(1, self.__x_max__()))
        return coord


def build_dungeon(settings: Settings):
    """
    Build a dungone object with the floor built


    Parameters
    ----------
    settings : Settings
        Game settings
    """
    dun = Dungeon(settings)
    floor = _build_empty_floor(settings)
    rooms = _build_rooms(settings)
    _add_rooms_to_floor(floor, rooms)

    hardnesses = []
    for row in floor:
        row_arr = []
        for tile in row:
            row_arr.append(tile.hardness)
        hardnesses.append(row_arr)

    _add_halls(settings, rooms, floor)

    dun.floor = floor
    dun.rooms = rooms
    return dun


def _build_empty_floor(settings: Settings):
    floor = []

    for y in range(0, settings.rows):
        row = []
        for x in range(0, settings.cols):
            if y == 0 or y == settings.rows - 1 \
                    or x == 0 or x == settings.cols - 1:
                row.append(Tile(TileType.BORDER,
                                settings.max_hardness))

            else:
                row.append(Tile(TileType.ROCK,
                                random.randint(1, settings.max_hardness - 1)))
        floor.append(row)
    return floor


def _build_rooms(settings: Settings):
    rooms = []

    attempts = 1
    while len(rooms) < (settings.min_rooms +
                        random.randint(0, settings.buffer_rooms)):
        new_room = build_rand_room(settings)
        if _is_valid_room(new_room, rooms, settings.rows, settings.cols):
            rooms.append(new_room)
        if attempts % 10000 == 0:
            rooms = []
        attempts += 1
    return rooms


def _is_valid_room(new_room: Room, rooms: List[Room], y_max: int,
                   x_max: int) -> bool:
    is_valid = True
    for room in rooms:
        is_valid &= not room.overlaps(new_room)
    is_valid &= (new_room.coordinate.x > 1 and
                 new_room.coordinate.x + new_room.width < x_max and
                 new_room.coordinate.y > 1 and
                 new_room.coordinate.y + new_room.height < y_max)
    return is_valid


def _add_rooms_to_floor(floor: List[List[Tile]], rooms: List[Room]):
    for room in rooms:
        for coord in room_to_points(room):
            floor[coord.y][coord.x] = Tile(TileType.ROOM, 0)


def _add_halls(settings: Settings, rooms: List[Room],
               floor: List[List[Tile]]) -> List[List[Tile]]:
    for i in range(0, len(rooms)):
        get_path_from(rooms[i - 1].coordinate, rooms[i].coordinate, floor)


def get_path_from(start: Coordinate, end: Coordinate, floor: List[List[Tile]]):
    points = get_dist_map(start, end, floor)

    current = points[end]
    while current != start:
        _set_tile_as_hall(floor, current)
        current = points[current]


def _set_tile_as_hall(floor: List[List[Tile]], coord: Coordinate):
    if floor[coord.y][coord.x].tile_type != TileType.ROOM:
        floor[coord.y][coord.x] = Tile(TileType.HALL, 0)


@dataclass(order=True)
class HeapPair:
    coord: Coordinate = field(compare=False)
    priority: int


def get_dist_map(start: Coordinate, end: Coordinate, floor: List[List[Tile]]):
    frontier = PriorityQueue()
    frontier.put(HeapPair(start, 0))
    came_from = {}
    cost_so_far = {}
    came_from[start] = None
    cost_so_far[start] = 0

    while not frontier.empty():
        current = frontier.get().coord

        if current == end:
            break

        successors = filter(lambda x: is_valid_point(x, floor),
                            get_neighbors(current))

        for next in successors:
            new_cost = (cost_so_far[current] + 1 +
                        floor[next.y][next.x].hardness)
            if next not in cost_so_far or new_cost < cost_so_far[next]:
                cost_so_far[next] = new_cost
                priority = new_cost + _heuristic(end, next)
                frontier.put(HeapPair(next, priority))
                came_from[next] = current
    return came_from


def is_valid_point(point: Coordinate, floor: List[List[Tile]]) -> bool:
    return (point.y > 0 and point.y < len(floor) - 1
            and point.x > 0 and point.x < len(floor[0]) - 1)


def _heuristic(goal: Coordinate, current: Coordinate) -> float:
    return sqrt(pow(abs(goal.x - current.x), 2) +
                pow(abs(goal.y - current.y), 2))
