from serpent_king.objs.coordinate import Coordinate
from serpent_king.objs.settings import Settings
import random


class Room:
    def __init__(self, coordinate: Coordinate, height: int, width: int):
        self.coordinate = coordinate
        self.height = height
        self.width = width

    def overlaps(self, other_room: 'Room') -> bool:
        """
        Does this room touch the other_room with a 1 tile buffer?

        Parameters
        ----------
        other_room : Room
            the room to compare to
        """
        self_points = _room_to_points_border(self)
        other_points = _room_to_points_border(other_room)

        return not self_points.isdisjoint(other_points)


def _room_to_points_border(room: Room) -> set:
    """
    Converts a room obj to a set of Coordinates with a one tile
    border included

    Parameters
    ----------
    room : Room
        Room to convert

    Returns
    -------
    set:
        set of coordinates in the room
    """
    ret = set()
    for y in range(room.coordinate.y - 1, room.coordinate.y + room.height):
        for x in range(room.coordinate.x - 1, room.coordinate.x + room.width):
            ret.add(Coordinate(y, x))
    return ret

def room_to_points(room: Room) -> set:
    """
    Converts a room obj to a set of Coordinates

    Parameters
    ----------
    room : Room
        Room to convert

    Returns
    -------
    set:
        set of coordinates in the room
    """
    ret = set()
    for y in range(room.coordinate.y, room.coordinate.y + room.height):
        for x in range(room.coordinate.x, room.coordinate.x + room.width):
            ret.add(Coordinate(y, x))
    return ret




def build_rand_room(settings: Settings) -> Room:
    """
    Build a random room (random height, width, position)
    based on the settings input

    Parameters
    ----------
    settings : Settings
        game settings

    Returns
    -------
    Room:
        New random room
    """
    coord = Coordinate(random.randint(1, settings.rows - 1),
                       random.randint(1, settings.cols - 1))
    return Room(coord, settings.min_room_height + random.randint(0,
                                       settings.room_height_buffer),
                settings.min_room_width +random.randint(0,
                               settings.room_width_buffer))
