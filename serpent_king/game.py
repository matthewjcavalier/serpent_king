from serpent_king.objs.dungeon import Dungeon
from serpent_king.objs.settings import Settings
import serpent_king.display as display
from serpent_king.display import IO
from serpent_king.character.character import build_characters, Player,Character, Npc
from dataclasses import dataclass
from typing import List


@dataclass
class GameState:
    dun: Dungeon
    npcs: List[Character]
    pc: Player


def game_loop(settings: Settings, dun: Dungeon):
    characters = build_characters(settings, dun)
    game_state = GameState(dun, characters["npcs"], characters["pc"])
    _give_characters_locations(game_state)

    view = display.IO(settings.rows, settings.cols)
    try:
        display.draw_dungeon(view, dun)
        _draw_character(game_state.pc, view)
        for character in game_state.npcs:
            _draw_character(character, view)

        view.center_on(game_state.pc.get_y(), game_state.pc.get_x())
        view.refresh()

        key_pressed = "a"
        while key_pressed != "Q":
            key_pressed = view.get_key()
            view.set_spot(0, 0, key_pressed)

            if is_direction(key_pressed):
                y_diff = direction_to_y(key_pressed)
                x_diff = direction_to_x(key_pressed)
                view.move_dun_pad_view(y_diff, x_diff)
    finally:
        view.close()


def _draw_character(character: Character, io: IO):
    io.set_game_pad_spot(character.get_y(), character.get_x(), character.get_symbol())


def _give_characters_locations(game_state: GameState):
    game_state.pc.coord = game_state.dun.get_rand_open_loc()

    for npc in game_state.npcs:
        npc.coord = game_state.dun.get_rand_open_loc()


def is_direction(text):
    return text in {"KEY_UP", "KEY_DOWN", "KEY_LEFT", "KEY_RIGHT"}


def direction_to_x(direction):
    return {"KEY_UP": 0,
            "KEY_DOWN": 0,
            "KEY_LEFT": -1,
            "KEY_RIGHT": 1}[direction]


def direction_to_y(direction):
    return {"KEY_UP": -1,
            "KEY_DOWN": 1,
            "KEY_LEFT": 0,
            "KEY_RIGHT": 0}[direction]
