from serpent_king.objs.dungeon import Dungeon
from serpent_king.objs.settings import Settings
from serpent_king.objs.tile import TileType, Tile
from typing import List
from math import floor as floor_fun
import curses


def draw_dun(dungeon: Dungeon):
    rows = get_display_symbols(dungeon)
    for row in rows:
        print(row)


def draw_hardness(grid: List[List[int]]):
    rows = get_hardness_symbols(grid)
    for row in rows:
        print(row)


def get_display_symbols(dungeon: Dungeon) -> List[str]:
    floor = dungeon.floor
    display_symbols = []
    for row in floor:
        row_str = ""
        for tile in row:
            row_str += _get_tile_symbol(dungeon.settings, tile)
        display_symbols.append(row_str)
    return display_symbols


def get_hardness_symbols(grid: List[List[int]]) -> List[str]:
    rows = []
    for row in grid:
        row_str = ""
        for spot in row:
            row_str += f"{spot % 10}"
        rows.append(row_str)
    return rows


def _get_tile_symbol(settings: Settings, tile: Tile):
    if(tile.tile_type == TileType.BORDER):
        return settings.border_char
    if(tile.tile_type == TileType.ROCK):
        return settings.rock_char
    if(tile.tile_type == TileType.HALL):
        return settings.hall_char
    if(tile.tile_type == TileType.ROOM):
        return settings.room_char
    return "%"


class IO:
    def __init__(self, dun_height: int, dun_width: int):
        self.screen = curses.initscr()
        curses.curs_set(0)
        curses.noecho()
        curses.cbreak()
        self.screen.keypad(True)
        self.screen.clear()
        self.game_area_viewport_x = 2
        self.game_area_viewport_y = 2
        self.game_area_viewport_height = 20
        self.game_area_viewport_width = 80
        self.dun_pad = curses.newpad(dun_height + 1, dun_width + 1)
        self.dun_pad_x = 0
        self.dun_pad_y = 0
        self.dun_width = dun_width
        self.dun_height = dun_height

    def get_key(self):
        return self.screen.getkey()

    def close(self):
        curses.nocbreak()
        self.screen.keypad(False)
        curses.echo()
        curses.curs_set(1)
        curses.endwin()

    def center_on(self, y, x):
        view_port_x_offset = floor_fun(self.game_area_viewport_width / 2)
        view_port_y_offset = floor_fun(self.game_area_viewport_height / 2)
        self.dun_pad_x = max(0, x - view_port_x_offset)
        self.dun_pad_y = max(0, y - view_port_y_offset)

    def refresh(self):
        self._draw_debug_info(self.game_area_viewport_height +
                              self.game_area_viewport_y, 0)
        self.screen.refresh()
        self.dun_pad.refresh(self.dun_pad_y,
                             self.dun_pad_x,
                             self.game_area_viewport_y,
                             self.game_area_viewport_x,
                             self.game_area_viewport_height +
                             self.game_area_viewport_y,
                             self.game_area_viewport_width +
                             self.game_area_viewport_x)

    def _draw_debug_info(self, y, x):
        counter = 1
        for key in self.__dict__.keys():
            self.set_spot(y + counter, x, f"({key}: {self.__dict__[key]}")
            counter += 1

    def set_spot(self, y: int, x: int, string: str):
        """
        write string to screen at location

        Parameters
        ----------
        y : int
            y location (see row)
        x : int
            x location (see column)
        string : str
            string to write
        """
        self.screen.addstr(y, x, string)

    def set_game_pad_spot(self, y: int, x: int, string: str):
        """
        write string to game pad at location

        Parameters
        ----------
        y : int
            y location (see row)
        x : int
            x location (see column)
        string : str
            string to write
        """
        self.dun_pad.addstr(y, x, string)

    def move_dun_pad_view(self, y_diff: int, x_diff: int):
        self.dun_pad_y = min(max(y_diff + self.dun_pad_y, 0), self.dun_height)
        self.dun_pad_x = min(max(x_diff + self.dun_pad_x, 0), self.dun_width)
        self.refresh()

    def update_game_area(self, y: int, x: int, string: str):
        self.set_game_pad_spot(y, x, string)


def draw_dungeon(io: IO, dungeon: Dungeon):
    for y in range(0, len(dungeon.floor)):
        for x in range(0, len(dungeon.floor[0])):
            io.update_game_area(y, x,
                                _get_tile_symbol(dungeon.settings,
                                                 dungeon.floor[y][x]))
    io.refresh()
