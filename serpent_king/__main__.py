from serpent_king.objs.dungeon import build_dungeon
import serpent_king.objs.settings as s
import random
import serpent_king.game as g


def main():
    # random.seed(1234)
    # settings = s.build_settings()
    # settings = s.build_tiny()
    settings = s.build_big()
    dun = build_dungeon(settings)
    # display.draw_dun(dun)
    g.game_loop(settings, dun)


if __name__ == "__main__":
    main()
