from serpent_king.objs.dungeon import Dungeon
from serpent_king.objs.coordinate import Coordinate
from serpent_king.objs.settings import Settings


class Character:
    def __init__(self):
        self.health = None
        self.symbol = "!"
        self.coord = None

    def get_y(self):
        return self.coord.y

    def get_x(self):
        return self.coord.x

    def get_symbol(self):
        return self.symbol


class Player(Character):
    def __init__(self, max_hp: int, symbol: str, dungeon: Dungeon):
        self.health = max_hp
        self.symbol = symbol
        self.coord: Coordinate = None


class Npc(Character):
    def __init__(self, max_hp: int, symbol: str, dungeon: Dungeon):
        self.health = max_hp
        self.symbol = symbol
        self.coord: Coordinate = None


def build_characters(settings: Settings, dun: Dungeon):
    characters = {}
    characters["pc"] = Player(settings.player_hp, settings.player_symbol, dun)

    characters["npcs"] = []

    return characters
